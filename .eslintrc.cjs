/*
 * @Author       : yinzhishan
 * @Date         : 2022-08-03 16:09:00
 * @FilePath     : \ShopkeeperMallPad\shopkeeperMallPad\.eslintrc.cjs
 * @Description  : Do not edit
 */
module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-essential',
        'plugin:prettier/recommended',
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    plugins: ['vue'],
    rules: {},
};
