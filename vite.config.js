/*
 * @Author       : yinzhishan
 * @Date         : 2022-08-03 15:59:09
 * @FilePath     : \shopkeeperMallPad\vite.config.js
 * @Description  : Do not edit
 */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [VantResolver()],
    }),
  ],
  server: {
    port: 8083,
  },
});
