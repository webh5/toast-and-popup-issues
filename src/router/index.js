/*
 * @Author       : yinzhishan
 * @Date         : 2022-08-03 17:32:36
 * @FilePath     : \shopkeeperMallPad\src\router\index.js
 * @Description  : Do not edit
 */
import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "shop" */ '../views/home.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/about.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
