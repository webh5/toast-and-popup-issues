/*
 * @Author       : yinzhishan
 * @Date         : 2022-08-03 15:59:09
 * @FilePath     : \shopkeeperMallPad\src\main.js
 * @Description  : Do not edit
 */
import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { Button, Toast, Popup } from 'vant';
import 'vant/lib/index.css'; // 全局引入样式
createApp(App).use(Button).use(Toast).use(Popup).use(store).use(router).mount('#app');
