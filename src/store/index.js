/*
 * @Author       : yinzhishan
 * @Date         : 2021-02-05 11:15:19
 * @FilePath     : \shopkeeperMallPad\src\store\index.js
 * @Description  : Do not edit
 */
import { createStore } from 'vuex';

export default createStore({
  state: {
    count: 0,
  },
  actions: {},
  mutations: {
    addCount(state, res) {
      state.count += res;
    },
  },
  getters: {
    getCount(state) {
      return state.count;
    },
  },
});
